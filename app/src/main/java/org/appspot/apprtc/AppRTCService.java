package org.appspot.apprtc;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.PowerManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.DefaultVideoDecoderFactory;
import org.webrtc.DefaultVideoEncoderFactory;
import org.webrtc.EglBase;
import org.webrtc.FileVideoCapturer;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SessionDescription;
import org.webrtc.SoftwareVideoDecoderFactory;
import org.webrtc.SoftwareVideoEncoderFactory;
import org.webrtc.StatsReport;
import org.webrtc.SurfaceTextureHelper;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoDecoderFactory;
import org.webrtc.VideoEncoderFactory;
import org.webrtc.VideoFrame;
import org.webrtc.VideoSink;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;
import org.webrtc.audio.AudioDeviceModule;
import org.webrtc.audio.JavaAudioDeviceModule;
import org.webrtc.audio.LegacyAudioDeviceModule;
import org.webrtc.voiceengine.WebRtcAudioManager;
import org.webrtc.voiceengine.WebRtcAudioRecord;
import org.webrtc.voiceengine.WebRtcAudioTrack;
import org.webrtc.voiceengine.WebRtcAudioUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AppRTCService extends Service implements WebSocketClient.WebSocketChannelEvents, PeerConnectionClient.PeerConnectionEvents {

    private static final String TAG = "AppRTCService";

    private static final boolean DEBUG = true;
    private static final int APPRTC_ID = 66;
    private static final String VIDEO_TRACK_ID = "ARDAMSv0";
    private static final String AUDIO_TRACK_ID = "ARDAMSa0";
    private static final String AUDIO_ECHO_CANCELLATION_CONSTRAINT = "googEchoCancellation";
    private static final String AUDIO_AUTO_GAIN_CONTROL_CONSTRAINT = "googAutoGainControl";
    private static final String AUDIO_HIGH_PASS_FILTER_CONSTRAINT = "googHighpassFilter";
    private static final String AUDIO_NOISE_SUPPRESSION_CONSTRAINT = "googNoiseSuppression";

    private static final ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();

    private final ProxyVideoSink mRemoteProxyRenderer = new ProxyVideoSink();
    private final ProxyVideoSink mLocalProxyVideoSink = new ProxyVideoSink();
    private final EglBase mEglBase = EglBase.create();
    private final List<VideoSink> mRemoteSinks = new ArrayList<>();
    private final Object mLock = new Object();

    private boolean mVideoCallEnabled;
    private boolean mUseCamera2;
    private String mVideoCodec;
    private String mAudioCodec;
    private boolean mHWCodec;
    private boolean mCaptureToTexture;
    private boolean mFlexfecEnabled;
    private boolean mNoAudioProcessing;
    private boolean mAecDump;
    private boolean mSaveInputAudioToFile;
    private boolean mUseOpenSLES;
    private boolean mDisableBuiltInAEC;
    private boolean mDisableBuiltInAGC;
    private boolean mDisableBuiltInNS;
    private boolean mDisableWebRtcAGCAndHPF;
    private int mVideoWidth;
    private int mVideoHeight;
    private int mCameraFps;
    private int mVideoStartBitrate;
    private int mAudioStartBitrate;
    private boolean mTracing;
    private boolean mRtcEventLogEnabled;
    private boolean mUseLegacyAudioDevice;
    private boolean mDataChannelEnabled;
    private boolean mOrdered;
    private boolean mNegotiated;
    private int mMaxRetrMs;
    private int mMaxRetr;
    private int mId;
    private String mProtocol;
    private String mVideoFileAsCamera;
    private boolean mLoopback;

    private Resources mResources;
    private MessageHandler mHandler;
    private HandlerThread mWebSocketThread;
    private Handler mWebSocketHandler;
    private HandlerThread mCameraThread;
    private Handler mCameraHandler;
    private ConnectivityManager mConnectivityManager;
    private WebSocketClient mWebSocketClient;
    private PeerConnectionFactory.Options mOptions;
    private RecordedAudioToFileController saveRecordedAudioToFile = null;
    private PeerConnectionClient.PeerConnectionParameters mPeerConnectionParameters;
    private PeerConnectionFactory mFactory;
    private SurfaceTextureHelper mSurfaceTextureHelper;
    private VideoTrack mLocalVideoTrack;
    private VideoSource mVideoSource;
    private AudioTrack mLocalAudioTrack;
    private AudioSource mAudioSource;
    private MediaConstraints mAudioConstraints;
    private VideoCapturer mVideoCapturer;
    private ArrayList<PeerConnectionItem> mConnections;
    private AppRTCAudioManager mAudioManager;
    private List<PeerConnection.IceServer> mIceServers;
    private ExecutorService mReleaseExecutor;
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock;

    private String mSocketId;
    private String mWssUrlStr;
    private String mStunUrlStr;
    private String mUdpTurnUrlStr;
    private String mTcpTurnUrlStr;
    private String mUserName;
    private String mCredential;
    private String mDeviceId;

    private boolean isStart;
    private boolean isPreferIsac;
    private boolean isVideoCapturerStopped;
    private boolean isWebSocketConnected;
    private boolean isPeerConnected;
    private long mCallStartedTimeMs;
    private WifiManager mWifiManager;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate()...");
        if (Build.VERSION.SDK_INT <= 26) {
            setPermissions(this);
        }

        mWifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

        isStart = false;
        isVideoCapturerStopped = true;
        isWebSocketConnected = false;
        isPeerConnected = false;

        mResources = getResources();
        mDeviceId = getDeviceId();
        mRemoteSinks.add(mRemoteProxyRenderer);
        mHandler = new MessageHandler(this, getMainLooper());
        mConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = mPowerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "apprtc");
        mWakeLock.acquire();

        initParameters();
        initIceServers();
        initHandlers();

        PeerConnectionClient.DataChannelParameters dataChannelParameters = null;
        if (mDataChannelEnabled) {
            dataChannelParameters = new PeerConnectionClient.DataChannelParameters(mOrdered,
                    mMaxRetrMs, mMaxRetr, mProtocol, mNegotiated, mId);
        }
        mPeerConnectionParameters =
                new PeerConnectionClient.PeerConnectionParameters(mVideoCallEnabled, mLoopback,
                        mTracing, mVideoWidth, mVideoHeight, mCameraFps, mVideoStartBitrate, mVideoCodec,
                        mHWCodec, mFlexfecEnabled, mAudioStartBitrate, mAudioCodec, mNoAudioProcessing,
                        mAecDump, mSaveInputAudioToFile, mUseOpenSLES, mDisableBuiltInAEC, mDisableBuiltInAGC,
                        mDisableBuiltInNS, mDisableWebRtcAGCAndHPF, mRtcEventLogEnabled, mUseLegacyAudioDevice, dataChannelParameters);
        mOptions = new PeerConnectionFactory.Options();
        if (mLoopback) {
            mOptions.networkIgnoreMask = 0;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand=>isStart: " + isStart);
        if (!isStart) {
            if (isNetworkActive()) {
                startCall();
            } else {
                Log.d(TAG, "onStartCommand=>network not active.");
                mHandler.sendEmptyMessageDelayed(MessageHandler.MSG_RECALL, MessageHandler.RECALL_DELAYED);
            }
            startForeground();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy()...");
        mWakeLock.release();
        release();
        stopForeground(true);
        super.onDestroy();
    }
    public void release() {
        Log.d(TAG, "release()...");
        disconnect("release");
        EXECUTOR_SERVICE.execute(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Stopping capture.");
                mEglBase.release();
            }
        });
        if (mWebSocketThread != null) {
            mWebSocketThread.quit();
            mWebSocketThread = null;
            mWebSocketHandler = null;
        }
    }

    public void disconnect(String description) {
        Log.d(TAG, "disconnect=>description: " + description
                + " isWebSocketConnected: " + isWebSocketConnected);
        removeAllMessages();
        stopVideoSource();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (mLock) {
                    PeerConnectionItem item = null;
                    if (mConnections != null) {
                        Log.d(TAG, "remove connections, size: " + mConnections.size());
                        for (int i = 0; i < mConnections.size(); i++) {
                            item = mConnections.get(i);
                            if (item != null && item.peerConnectionClient != null) {
                                item.peerConnectionClient.close();
                            }
                            item.peerConnectionClient = null;
                            mConnections.remove(i);
                        }
                        mConnections = null;
                    }
                }
            }
        });

        if (mAudioManager != null) {
            Log.d(TAG, "stop audio manager.");
            mAudioManager.stop();
            mAudioManager = null;
        }

        if (mWebSocketHandler != null) {
            mWebSocketHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mWebSocketClient != null) {
                        Log.d(TAG, "disconnect websocket.");
                        if (isWebSocketConnected) {
                            mWebSocketClient.disconnect(true);
                        }
                        mWebSocketClient = null;
                    }
                }
            });
        }

        Log.d(TAG, "executor=>isShutdown: " + EXECUTOR_SERVICE.isShutdown() + " isTerminated: " + EXECUTOR_SERVICE.isTerminated());
        if (mReleaseExecutor != null) {
            mReleaseExecutor.shutdownNow();
            mReleaseExecutor = null;
        }
        mReleaseExecutor = Executors.newSingleThreadExecutor();
        mReleaseExecutor.execute(new Runnable() {
            @Override
            public void run() {
                closeInternal();
            }
        });

        if (mCameraThread != null) {
            Log.d(TAG, "quit camera thread.");
            mCameraThread.quit();
            mCameraThread = null;
            mCameraHandler = null;
        }

        isStart = false;
        isWebSocketConnected = false;
    }

    private void closeInternal() {
        Log.d(TAG, "closeInternal=>isPeerConnected: " + isPeerConnected);
        if (mFactory != null && mAecDump) {
            Log.d(TAG, "stop ace dump.");
            if (isPeerConnected) {
                mFactory.stopAecDump();
            }
        }
        if (mLocalAudioTrack != null) {
            Log.d(TAG, "dispose local audio track.");
            mLocalAudioTrack.dispose();
            mLocalAudioTrack = null;
        }
        Log.d(TAG, "Closing audio source.");
        if (mAudioSource != null) {
            mAudioSource.dispose();
            mAudioSource = null;
        }
        if (mVideoCapturer != null) {
            Log.d(TAG, "close video Capturer.");
            try {
                if (!isVideoCapturerStopped) {
                    mVideoCapturer.stopCapture();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            isVideoCapturerStopped = true;
            mVideoCapturer.dispose();
            mVideoCapturer = null;
        }
        isVideoCapturerStopped = true;
        if (mLocalVideoTrack != null) {
            Log.d(TAG, "dispose local video track!");
            mLocalVideoTrack.dispose();
            mLocalVideoTrack = null;
        }
        if (mSurfaceTextureHelper != null) {
            Log.d(TAG, "dispose surface texture helper.");
            mSurfaceTextureHelper.dispose();
            mSurfaceTextureHelper = null;
        }
        if (saveRecordedAudioToFile != null) {
            Log.d(TAG, "Closing audio file for recorded input audio.");
            saveRecordedAudioToFile.stop();
            saveRecordedAudioToFile = null;
        }
        Log.d(TAG, "Closing peer connection factory.");
        if (mFactory != null) {
            mFactory.dispose();
            mFactory = null;
        }
        if (isPeerConnected) {
            PeerConnectionFactory.stopInternalTracingCapture();
            PeerConnectionFactory.shutdownInternalTracer();
        }
        isPeerConnected = false;
    }

    public String getDeviceId() {
        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        return tm.getDeviceId();
    }

    private void removeAllMessages() {
        mHandler.removeMessages(MessageHandler.MSG_RECALL);
        mHandler.removeMessages(MessageHandler.MSG_SEND_STATE);
        mHandler.removeMessages(MessageHandler.MSG_CONNECTED_TIMEOUT);
    }

    private void startForeground() {
        Notification notify = new Notification();
        startForeground(APPRTC_ID, notify);
    }

    private void initParameters() {
        mWssUrlStr = mResources.getString(R.string.wss_url);
        mStunUrlStr = mResources.getString(R.string.stun_url);
        mUdpTurnUrlStr = mResources.getString(R.string.udp_turn_url);
        mTcpTurnUrlStr = mResources.getString(R.string.tcp_turn_url);
        mUserName = mResources.getString(R.string.user_name);
        mCredential = mResources.getString(R.string.credential);

        mVideoCallEnabled = mResources.getBoolean(R.bool.video_call_enabled);
        mUseCamera2 = mResources.getBoolean(R.bool.use_camera2);
        mVideoCodec = mResources.getString(R.string.video_codec);
        mAudioCodec = mResources.getString(R.string.audio_codec);
        mHWCodec = mResources.getBoolean(R.bool.hw_codec);
        mCaptureToTexture = mResources.getBoolean(R.bool.capture_to_texture);
        mFlexfecEnabled = mResources.getBoolean(R.bool.flex_fec_enabled);
        mNoAudioProcessing = mResources.getBoolean(R.bool.no_audio_processing);
        mAecDump = mResources.getBoolean(R.bool.aec_dump);
        mSaveInputAudioToFile = mResources.getBoolean(R.bool.save_input_audio_to_file);
        mUseOpenSLES = mResources.getBoolean(R.bool.use_opensles);
        mDisableBuiltInAEC = mResources.getBoolean(R.bool.disable_built_in_aec);
        mDisableBuiltInAGC = mResources.getBoolean(R.bool.disable_built_in_agc);
        mDisableBuiltInNS = mResources.getBoolean(R.bool.disable_built_in_ns);
        mDisableWebRtcAGCAndHPF = mResources.getBoolean(R.bool.disable_webrtc_agc_and_hpf);
        mVideoWidth = mResources.getInteger(R.integer.video_width);
        mVideoHeight = mResources.getInteger(R.integer.video_height);
        mCameraFps = mResources.getInteger(R.integer.camera_fps);
        mVideoStartBitrate = mResources.getInteger(R.integer.video_start_bitrate);
        mAudioStartBitrate = mResources.getInteger(R.integer.audio_start_bitrate);
        mTracing = mResources.getBoolean(R.bool.tracing);
        mRtcEventLogEnabled = mResources.getBoolean(R.bool.rtc_event_log_enabled);
        mUseLegacyAudioDevice = mResources.getBoolean(R.bool.use_legacy_audio_device);
        mDataChannelEnabled = mResources.getBoolean(R.bool.data_channel_enabled);
        mOrdered = mResources.getBoolean(R.bool.ordered);
        mNegotiated = mResources.getBoolean(R.bool.negotiated);
        mMaxRetrMs = mResources.getInteger(R.integer.max_retr_ms);
        mMaxRetr = mResources.getInteger(R.integer.max_retr);
        mId = mResources.getInteger(R.integer.id);
        mProtocol = mResources.getString(R.string.protocol);
        mVideoFileAsCamera = getVideoFileAsCamera();
        mLoopback = mResources.getBoolean(R.bool.loopback);
    }

    private void initHandlers() {
        mWebSocketThread = new HandlerThread("websocket");
        mWebSocketThread.start();
        mWebSocketHandler = new Handler(mWebSocketThread.getLooper());
    }
    private void initFactory() {
        final String fieldTrials = getFieldTrials();
        EXECUTOR_SERVICE.execute(() -> {
            Log.d(TAG, "Initialize WebRTC. Field trials: " + fieldTrials);
            PeerConnectionFactory.initialize(
                    PeerConnectionFactory.InitializationOptions.builder(this)
                            .setFieldTrials(fieldTrials)
                            .setEnableInternalTracer(true)
                            .createInitializationOptions());
        });

        if (mTracing) {
            PeerConnectionFactory.startInternalTracingCapture(
                    Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator
                            + "webrtc-trace.txt");
        }

        // Check if ISAC is used by default.
        isPreferIsac = mAudioCodec != null
                && mAudioCodec.equals(PeerConnectionClient.AUDIO_CODEC_ISAC);

        // It is possible to save a copy in raw PCM format on a file by checking
        // the "Save input audio to file" checkbox in the Settings UI. A callback
        // interface is set when this flag is enabled. As a result, a copy of recorded
        // audio samples are provided to this client directly from the native audio
        // layer in Java.
        if (mSaveInputAudioToFile) {
            if (!mUseOpenSLES) {
                Log.d(TAG, "Enable recording of microphone input audio to file");
                saveRecordedAudioToFile = new RecordedAudioToFileController(EXECUTOR_SERVICE);
            } else {
                // TODO(henrika): ensure that the UI reflects that if OpenSL ES is selected,
                // then the "Save inut audio to file" option shall be grayed out.
                Log.e(TAG, "Recording of input audio is not supported for OpenSL ES");
            }
        }

        final AudioDeviceModule adm = mUseLegacyAudioDevice
                ? createLegacyAudioDevice()
                : createJavaAudioDevice();

        // Create peer connection factory.
        if (mOptions != null) {
            Log.d(TAG, "Factory networkIgnoreMask option: " + mOptions.networkIgnoreMask);
        }
        final boolean enableH264HighProfile =
                PeerConnectionClient.VIDEO_CODEC_H264_HIGH.equals(mVideoCodec);
        final VideoEncoderFactory encoderFactory;
        final VideoDecoderFactory decoderFactory;

        if (mHWCodec) {
            encoderFactory = new DefaultVideoEncoderFactory(
                    mEglBase.getEglBaseContext(), true /* enableIntelVp8Encoder */, enableH264HighProfile);
            decoderFactory = new DefaultVideoDecoderFactory(mEglBase.getEglBaseContext());
        } else {
            encoderFactory = new SoftwareVideoEncoderFactory();
            decoderFactory = new SoftwareVideoDecoderFactory();
        }

        mFactory = PeerConnectionFactory.builder()
                .setOptions(mOptions)
                .setAudioDeviceModule(adm)
                .setVideoEncoderFactory(encoderFactory)
                .setVideoDecoderFactory(decoderFactory)
                .createPeerConnectionFactory();
        Log.d(TAG, "Peer connection factory created.");
        adm.release();

        if (mAecDump) {
            try {
                ParcelFileDescriptor aecDumpFileDescriptor =
                        ParcelFileDescriptor.open(new File(Environment.getExternalStorageDirectory().getPath()
                                        + File.separator + "Download/audio.aecdump"),
                                ParcelFileDescriptor.MODE_READ_WRITE | ParcelFileDescriptor.MODE_CREATE
                                        | ParcelFileDescriptor.MODE_TRUNCATE);
                mFactory.startAecDump(aecDumpFileDescriptor.detachFd(), -1);
            } catch (IOException e) {
                Log.e(TAG, "Can not open aecdump file", e);
            }
        }

        if (saveRecordedAudioToFile != null) {
            if (saveRecordedAudioToFile.start()) {
                Log.d(TAG, "Recording input audio to file is activated");
            }
        }
    }

    private void initVideo() {
        mVideoSource = mFactory.createVideoSource(mVideoCapturer.isScreencast());
        mVideoCapturer.initialize(mSurfaceTextureHelper, this, mVideoSource.getCapturerObserver());
        mVideoCapturer.startCapture(mVideoWidth, mVideoHeight, mCameraFps);

        mLocalVideoTrack = mFactory.createVideoTrack(VIDEO_TRACK_ID, mVideoSource);
        mLocalVideoTrack.setEnabled(true);
        mLocalVideoTrack.addSink(mLocalProxyVideoSink);
        isVideoCapturerStopped = false;
    }

    private void initAudio() {
        mAudioConstraints = new MediaConstraints();
        if (mNoAudioProcessing) {
            Log.d(TAG, "Disabling audio processing");
            mAudioConstraints.mandatory.add(
                    new MediaConstraints.KeyValuePair(AUDIO_ECHO_CANCELLATION_CONSTRAINT, "false"));
            mAudioConstraints.mandatory.add(
                    new MediaConstraints.KeyValuePair(AUDIO_AUTO_GAIN_CONTROL_CONSTRAINT, "false"));
            mAudioConstraints.mandatory.add(
                    new MediaConstraints.KeyValuePair(AUDIO_HIGH_PASS_FILTER_CONSTRAINT, "false"));
            mAudioConstraints.mandatory.add(
                    new MediaConstraints.KeyValuePair(AUDIO_NOISE_SUPPRESSION_CONSTRAINT, "false"));
        }
        mAudioSource = mFactory.createAudioSource(mAudioConstraints);
        mLocalAudioTrack = mFactory.createAudioTrack(AUDIO_TRACK_ID, mAudioSource);
        mLocalAudioTrack.setEnabled(true);
    }

    public boolean isNetworkActive() {
        boolean isNetworkActive = false;
        NetworkInfo info = mConnectivityManager.getActiveNetworkInfo();
        if (info != null && info.isConnected() && info.isAvailable()) {
            isNetworkActive = true;
        }
        return isNetworkActive;
    }

    private void initIceServers() {
        mIceServers = new ArrayList<PeerConnection.IceServer>();
        PeerConnection.IceServer stunServer =
                PeerConnection.IceServer.builder(mStunUrlStr)
                        .setPassword("")
                        .createIceServer();
        mIceServers.add(stunServer);
        PeerConnection.IceServer udpServer =
                PeerConnection.IceServer.builder(mUdpTurnUrlStr)
                        .setUsername(mUserName)
                        .setPassword(mCredential)
                        .setTlsCertPolicy(PeerConnection.TlsCertPolicy.TLS_CERT_POLICY_INSECURE_NO_CHECK)
                        .createIceServer();
        mIceServers.add(udpServer);
        PeerConnection.IceServer tcpServer =
                PeerConnection.IceServer.builder(mTcpTurnUrlStr)
                        .setUsername(mUserName)
                        .setPassword(mCredential)
                        .setTlsCertPolicy(PeerConnection.TlsCertPolicy.TLS_CERT_POLICY_INSECURE_NO_CHECK)
                        .createIceServer();
        mIceServers.add(tcpServer);
    }

    private String getVideoFileAsCamera() {
        return null;
    }

    public void stopVideoSource() {
        EXECUTOR_SERVICE.execute(() -> {
            if (mVideoCapturer != null && !isVideoCapturerStopped) {
                Log.d(TAG, "Stop video source.");
                try {
                    mVideoCapturer.stopCapture();
                } catch (InterruptedException e) {
                }
                isVideoCapturerStopped = true;
            }
        });
    }

    public void startVideoSource() {
        EXECUTOR_SERVICE.execute(() -> {
            if (mVideoCapturer != null && isVideoCapturerStopped) {
                Log.d(TAG, "Restart video source.");
                mVideoCapturer.startCapture(mVideoWidth, mVideoHeight, mCameraFps);
                isVideoCapturerStopped = false;
            }
        });
    }

    private VideoCapturer createVideoCapturer() {
        final VideoCapturer videoCapturer;
        if (mVideoFileAsCamera != null) {
            try {
                videoCapturer = new FileVideoCapturer(mVideoFileAsCamera);
            } catch (IOException e) {
                reportError("Failed to open video file for emulated camera");
                return null;
            }
        } else if (useCamera2()) {
            if (!mCaptureToTexture) {
                reportError(getString(R.string.camera2_texture_only_error));
                return null;
            }

            Log.d(TAG, "Creating capturer using camera2 API.");
            videoCapturer = createCameraCapturer(new Camera2Enumerator(this));
        } else {
            Log.d(TAG, "Creating capturer using camera1 API.");
            videoCapturer = createCameraCapturer(new Camera1Enumerator(mCaptureToTexture));
        }
        if (videoCapturer == null) {
            reportError("Failed to open camera");
            return null;
        }
        return videoCapturer;
    }

    private VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();

        // Front facing camera not found, try something else
        Log.d(TAG, "Looking for other cameras.");
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Log.d(TAG, "Creating other camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // First, try to find front facing camera
        Log.d(TAG, "Looking for front facing cameras.");
        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                Log.d(TAG, "Creating front facing camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

    private boolean useCamera2() {
        return Camera2Enumerator.isSupported(this) && mUseCamera2;
    }

    private String getFieldTrials() {
        String fieldTrials = "";
        if (mFlexfecEnabled) {
            fieldTrials += PeerConnectionClient.VIDEO_FLEXFEC_FIELDTRIAL;
            Log.d(TAG, "Enable FlexFEC field trial.");
        }
        fieldTrials += PeerConnectionClient.VIDEO_VP8_INTEL_HW_ENCODER_FIELDTRIAL;
        if (mDisableWebRtcAGCAndHPF) {
            fieldTrials += PeerConnectionClient.DISABLE_WEBRTC_AGC_FIELDTRIAL;
            Log.d(TAG, "Disable WebRTC AGC field trial.");
        }
        return fieldTrials;
    }

    public void startCall() {
        Log.d(TAG, "startCall()...");
        mCallStartedTimeMs = System.currentTimeMillis();
        mHandler.sendEmptyMessageDelayed(MessageHandler.MSG_CONNECTED_TIMEOUT, MessageHandler.CONNECTED_TIMEOUT);

        mCameraThread = new HandlerThread("camera");
        mCameraThread.start();
        mCameraHandler = new Handler(mCameraThread.getLooper());
        mVideoCapturer = createVideoCapturer();
        mSurfaceTextureHelper = SurfaceTextureHelper.create(mCameraHandler, mEglBase.getEglBaseContext());
        mWebSocketClient = new WebSocketClient(mWebSocketHandler, this);
        initFactory();
        initVideo();
        initAudio();

        mWebSocketHandler.post(new Runnable() {
            @Override
            public void run() {
                mWebSocketClient.connect(mWssUrlStr, null, mDeviceId);
            }
        });
        // Create and audio manager that will take care of audio routing,
        // audio modes, audio device enumeration etc.
        mAudioManager = AppRTCAudioManager.create(this);
        // Store existing audio settings and change audio mode to
        // MODE_IN_COMMUNICATION for best possible VoIP performance.
        Log.d(TAG, "Starting the audio manager...");
        mAudioManager.start(new AppRTCAudioManager.AudioManagerEvents() {
            // This method will be called each time the number of available audio
            // devices has changed.
            @Override
            public void onAudioDeviceChanged(
                    AppRTCAudioManager.AudioDevice audioDevice, Set<AppRTCAudioManager.AudioDevice> availableAudioDevices) {
                onAudioManagerDevicesChanged(audioDevice, availableAudioDevices);
            }
        });
        isStart = true;
    }

    private void onAudioManagerDevicesChanged(
            final AppRTCAudioManager.AudioDevice device, final Set<AppRTCAudioManager.AudioDevice> availableDevices) {
        Log.d(TAG, "onAudioManagerDevicesChanged: " + availableDevices + ", "
                + "selected: " + device);
        // TODO(henrika): add callback handler.
    }

    private PeerConnectionClient createPeerConnectionClient() {
        startVideoSource();
        // Create peer connection client.
        PeerConnectionClient pcc = new PeerConnectionClient(
                this, mEglBase, mPeerConnectionParameters, this);
        pcc.setPeerConnectionFactory(mFactory);
        pcc.setSurfaceTextureHelper(mSurfaceTextureHelper);
        pcc.setVideoSource(mVideoSource);
        pcc.setLocalVideoTrack(mLocalVideoTrack);
        pcc.setPreferIsac(isPreferIsac);
        pcc.setSaveRecordedAudioToFile(saveRecordedAudioToFile);
        pcc.setAudioConstraints(mAudioConstraints);
        pcc.setAudioSource(mAudioSource);
        pcc.setLocalAudioTrack(mLocalAudioTrack);
        return pcc;
    }

    private void createOffer(PeerConnectionItem item) {
        if (!isWebSocketConnected) {
            Log.e(TAG, "createOffer=>websocket not connected");
        }
        Log.d(TAG, "createOffer()...");
        final long delta = System.currentTimeMillis() - mCallStartedTimeMs;

        AppRTCClient.SignalingParameters signalingParameters = new AppRTCClient.SignalingParameters(
                mIceServers, true, null, mWssUrlStr, null, null, null);

        Log.d(TAG, "Creating peer connection, delay=" + delta + "ms");
        Log.d(TAG, "onConnectedToRoomInternal=>video call enabled: " + mVideoCallEnabled);

        item.peerConnectionClient = createPeerConnectionClient();
        item.peerConnectionClient.setSocketId(item.socketId);
        item.peerConnectionClient.setCameraHandler(mCameraHandler);

        item.peerConnectionClient.createPeerConnection(
                mLocalProxyVideoSink, mRemoteSinks, mVideoCapturer, signalingParameters);
        // Create offer. Offer SDP will be sent to answering client in
        // PeerConnectionEvents.onLocalDescription event.
        item.peerConnectionClient.createOffer();
    }

    public void createAnswer(PeerConnectionItem item) {
        if (!isWebSocketConnected) {
            Log.e(TAG, "createOffer=>websocket not connected");
        }
        Log.d(TAG, "createAnswer()...");
        final long delta = System.currentTimeMillis() - mCallStartedTimeMs;

        AppRTCClient.SignalingParameters signalingParameters = new AppRTCClient.SignalingParameters(
                mIceServers, false, null, mWssUrlStr, null, item.remoteSdp, null);
        Log.d(TAG, "Creating peer connection, delay=" + delta + "ms");
        Log.d(TAG, "onConnectedToRoomInternal=>video call enabled: " + mVideoCallEnabled);

        item.peerConnectionClient = createPeerConnectionClient();
        item.peerConnectionClient.setSocketId(item.socketId);
        item.peerConnectionClient.setCameraHandler(mCameraHandler);
        item.peerConnectionClient.setRemoteDescription(item.remoteSdp);

        item.peerConnectionClient.createPeerConnection(
                mLocalProxyVideoSink, mRemoteSinks, mVideoCapturer, signalingParameters);
        if (signalingParameters.offerSdp != null) {
            item.peerConnectionClient.setRemoteDescription(signalingParameters.offerSdp);
            Log.d(TAG, "Creating ANSWER...");
            // Create answer. Answer SDP will be sent to offering client in
            // PeerConnectionEvents.onLocalDescription event.
            item.peerConnectionClient.createAnswer();
        }
        if (signalingParameters.iceCandidates != null) {
            // Add remote ICE candidates from room.
            for (IceCandidate iceCandidate : signalingParameters.iceCandidates) {
                item.peerConnectionClient.addRemoteIceCandidate(iceCandidate);
            }
        }
    }

    public void sendOfferSdp(PeerConnectionItem item) {
        if (!isWebSocketConnected) {
            Log.e(TAG, "createOffer=>websocket not connected");
        }
        final SessionDescription sdp = item.localSdp;
        final String socketId = item.socketId;
        final boolean isOffer = item.isOffer;
        mWebSocketHandler.post(new Runnable() {
            @Override
            public void run() {
                if (isOffer) {
                    mWebSocketClient.sendOfferSdp(sdp, socketId);
                } else {
                    mWebSocketClient.sendAnswerSdp(sdp, socketId);
                }
            }
        });
    }

    public void sendIceCandidate(IceCandidate candidate, String socketId) {
        if (!isWebSocketConnected) {
            Log.e(TAG, "createOffer=>websocket not connected");
        }
        mWebSocketHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (mLock) {
                    PeerConnectionItem item = null;
                    for (int i = 0; i < mConnections.size(); i++) {
                        item = mConnections.get(i);
                        mWebSocketClient.sendIceCandidate(candidate, socketId);
                    }
                }
            }
        });
    }

    public void sendHeartbeat() {
        if (!isWebSocketConnected) {
            Log.e(TAG, "createOffer=>websocket not connected");
        }
        mWebSocketHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mWebSocketClient != null) {
                    mWebSocketClient.sendHeartbeat(mSocketId);
                }
            }
        });
    }

    private void setDefaultWifi(String ssid, String password) {
        try {
            if (mWifiManager != null) {
                if (!openWifi()) {
                    return;
                }
                int netId = mWifiManager.addNetwork(createWifiConfig(ssid, password, WIFICIPHER_WPA));
                boolean enableNetwork = mWifiManager.enableNetwork(netId, true);
                mWifiManager.reconnect();
            }

        } catch (Throwable e) {

        }
    }

    private static final int WIFICIPHER_NOPASS = 0;
    private static final int WIFICIPHER_WEP = 1;
    private static final int WIFICIPHER_WPA = 2;

    public void setPermissions(Context context) {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setData(Uri.parse("package:" + context.getPackageName()));
        context.startActivity(intent);
    }

    private WifiConfiguration createWifiConfig(String ssid, String password, int type) {
        WifiConfiguration config = new WifiConfiguration();
        config.allowedAuthAlgorithms.clear();
        config.allowedGroupCiphers.clear();
        config.allowedKeyManagement.clear();
        config.allowedPairwiseCiphers.clear();
        config.allowedProtocols.clear();
        config.SSID = "\"" + ssid + "\"";
        WifiConfiguration tempConfig = isExist(ssid);
        if (tempConfig != null) {
            Log.d("chunlei", "createWifiConfig: ");
            mWifiManager.removeNetwork(tempConfig.networkId);
        }
        if (type == WIFICIPHER_NOPASS) {
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        } else if (type == WIFICIPHER_WEP) {
            config.hiddenSSID = true;
            config.wepKeys[0] = "\"" + password + "\"";
            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            config.wepTxKeyIndex = 0;
        } else if (type == WIFICIPHER_WPA) {
            config.preSharedKey = "\"" + password + "\"";
            config.hiddenSSID = true;
            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            config.status = WifiConfiguration.Status.ENABLED;
            Log.d("chunlei", "WIFICIPHER_WPA: ");
        }

        return config;
    }

    // 打开WIFI
    public boolean openWifi() {/*必须点击权限允许不然会失败*/
        boolean bRet = true;
        if (!mWifiManager.isWifiEnabled()) {
            bRet = mWifiManager.setWifiEnabled(true);
        }
        Log.d("chunlei", "bRet=: " + bRet);
        return bRet;
    }

    // 关闭WIFI
    public void closeWifi() {
        if (mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(false);
        }
    }

    private WifiConfiguration isExist(String ssid) {
        List<WifiConfiguration> configs = mWifiManager.getConfiguredNetworks();
        for (WifiConfiguration config : configs) {
            if (config.SSID.equals("\"" + ssid + "\"")) {
                return config;
            }
        }
        return null;
    }

    private AudioDeviceModule createLegacyAudioDevice() {
        // Enable/disable OpenSL ES playback.
        if (!mUseOpenSLES) {
            Log.d(TAG, "Disable OpenSL ES audio even if device supports it");
            WebRtcAudioManager.setBlacklistDeviceForOpenSLESUsage(true /* enable */);
        } else {
            Log.d(TAG, "Allow OpenSL ES audio if device supports it");
            WebRtcAudioManager.setBlacklistDeviceForOpenSLESUsage(false);
        }

        if (mDisableBuiltInAEC) {
            Log.d(TAG, "Disable built-in AEC even if device supports it");
            WebRtcAudioUtils.setWebRtcBasedAcousticEchoCanceler(true);
        } else {
            Log.d(TAG, "Enable built-in AEC if device supports it");
            WebRtcAudioUtils.setWebRtcBasedAcousticEchoCanceler(false);
        }

        if (mDisableBuiltInNS) {
            Log.d(TAG, "Disable built-in NS even if device supports it");
            WebRtcAudioUtils.setWebRtcBasedNoiseSuppressor(true);
        } else {
            Log.d(TAG, "Enable built-in NS if device supports it");
            WebRtcAudioUtils.setWebRtcBasedNoiseSuppressor(false);
        }

        WebRtcAudioRecord.setOnAudioSamplesReady(saveRecordedAudioToFile);

        // Set audio record error callbacks.
        WebRtcAudioRecord.setErrorCallback(new WebRtcAudioRecord.WebRtcAudioRecordErrorCallback() {
            @Override
            public void onWebRtcAudioRecordInitError(String errorMessage) {
                Log.e(TAG, "onWebRtcAudioRecordInitError: " + errorMessage);
                reportError(errorMessage);
            }

            @Override
            public void onWebRtcAudioRecordStartError(
                    WebRtcAudioRecord.AudioRecordStartErrorCode errorCode, String errorMessage) {
                Log.e(TAG, "onWebRtcAudioRecordStartError: " + errorCode + ". " + errorMessage);
                reportError(errorMessage);
            }

            @Override
            public void onWebRtcAudioRecordError(String errorMessage) {
                Log.e(TAG, "onWebRtcAudioRecordError: " + errorMessage);
                reportError(errorMessage);
            }
        });

        WebRtcAudioTrack.setErrorCallback(new WebRtcAudioTrack.ErrorCallback() {
            @Override
            public void onWebRtcAudioTrackInitError(String errorMessage) {
                Log.e(TAG, "onWebRtcAudioTrackInitError: " + errorMessage);
                reportError(errorMessage);
            }

            @Override
            public void onWebRtcAudioTrackStartError(
                    WebRtcAudioTrack.AudioTrackStartErrorCode errorCode, String errorMessage) {
                Log.e(TAG, "onWebRtcAudioTrackStartError: " + errorCode + ". " + errorMessage);
                reportError(errorMessage);
            }

            @Override
            public void onWebRtcAudioTrackError(String errorMessage) {
                Log.e(TAG, "onWebRtcAudioTrackError: " + errorMessage);
                reportError(errorMessage);
            }
        });

        return new LegacyAudioDeviceModule();
    }

    private AudioDeviceModule createJavaAudioDevice() {
        // Enable/disable OpenSL ES playback.
        if (!mUseOpenSLES) {
            Log.w(TAG, "External OpenSLES ADM not implemented yet.");
            // TODO(magjed): Add support for external OpenSLES ADM.
        }

        // Set audio record error callbacks.
        JavaAudioDeviceModule.AudioRecordErrorCallback audioRecordErrorCallback = new JavaAudioDeviceModule.AudioRecordErrorCallback() {
            @Override
            public void onWebRtcAudioRecordInitError(String errorMessage) {
                Log.e(TAG, "onWebRtcAudioRecordInitError: " + errorMessage);
                reportError(errorMessage);
            }

            @Override
            public void onWebRtcAudioRecordStartError(
                    JavaAudioDeviceModule.AudioRecordStartErrorCode errorCode, String errorMessage) {
                Log.e(TAG, "onWebRtcAudioRecordStartError: " + errorCode + ". " + errorMessage);
                reportError(errorMessage);
            }

            @Override
            public void onWebRtcAudioRecordError(String errorMessage) {
                Log.e(TAG, "onWebRtcAudioRecordError: " + errorMessage);
                reportError(errorMessage);
            }
        };

        JavaAudioDeviceModule.AudioTrackErrorCallback audioTrackErrorCallback = new JavaAudioDeviceModule.AudioTrackErrorCallback() {
            @Override
            public void onWebRtcAudioTrackInitError(String errorMessage) {
                Log.e(TAG, "onWebRtcAudioTrackInitError: " + errorMessage);
                reportError(errorMessage);
            }

            @Override
            public void onWebRtcAudioTrackStartError(
                    JavaAudioDeviceModule.AudioTrackStartErrorCode errorCode, String errorMessage) {
                Log.e(TAG, "onWebRtcAudioTrackStartError: " + errorCode + ". " + errorMessage);
                reportError(errorMessage);
            }

            @Override
            public void onWebRtcAudioTrackError(String errorMessage) {
                Log.e(TAG, "onWebRtcAudioTrackError: " + errorMessage);
                reportError(errorMessage);
            }
        };

        return JavaAudioDeviceModule.builder(this)
                .setSamplesReadyCallback(saveRecordedAudioToFile)
                .setUseHardwareAcousticEchoCanceler(!mDisableBuiltInAEC)
                .setUseHardwareNoiseSuppressor(!mDisableBuiltInNS)
                .setAudioRecordErrorCallback(audioRecordErrorCallback)
                .setAudioTrackErrorCallback(audioTrackErrorCallback)
                .createAudioDeviceModule();
    }

    private void reportError(final String description) {
        Log.d(TAG, "reportError=>description: " + description);
    }

    /*-------------WebSocketClient.WebSocketChannelEvents-------------*/
    @Override
    public void onWebSocketMessage(String message) {
        Log.d(TAG, "onWebSocketMessage=>message: " + message);
        isWebSocketConnected = true;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (mLock) {
                    try {
                        JSONObject json = new JSONObject(message);
                        String type = json.optString("eventName");
                        if (type.equals("_peers")) {
                            if (mConnections == null) {
                                mConnections = new ArrayList<PeerConnectionItem>();
                            }
                            JSONObject data = json.getJSONObject("data");
                            JSONArray ids = data.getJSONArray("connections");
                            for (int i = 0; i < ids.length(); i++) {
                                PeerConnectionItem item = new PeerConnectionItem();
                                item.socketId = ids.getString(i);
                                item.isOffer = true;
                                mConnections.add(item);
                            }
                            mSocketId = data.getString("you");
                            if (mConnections.size() > 0) {
                                for (int i = 0; i < mConnections.size(); i++) {
                                    createOffer(mConnections.get(i));
                                }
                            } else {
                                Log.e(TAG, "on room create.");
                            }
                            mHandler.sendEmptyMessage(MessageHandler.MSG_SEND_STATE);
                            mHandler.sendEmptyMessageDelayed(MessageHandler.MSG_RECALL, MessageHandler.RESTART_DELAYED);
                        } else if (type.equals("_new_peer")) {
                            if (mConnections == null) {
                                mConnections = new ArrayList<PeerConnectionItem>();
                            }
                            JSONObject data = json.getJSONObject("data");
                            PeerConnectionItem item = new PeerConnectionItem();
                            item.isOffer = false;
                            item.socketId = data.getString("socketId");
                            mConnections.add(item);
                        } else if (type.equals("_offer")) {
                            JSONObject data = json.getJSONObject("data");
                            String socketId = data.getString("socketId");
                            JSONObject sdp = data.getJSONObject("sdp");
                            String sdpDescription = sdp.getString("sdp");
                            String sdpType = sdp.getString("type");
                            SessionDescription offerSdp = new SessionDescription(
                                    SessionDescription.Type.fromCanonicalForm(sdpType.toUpperCase()), sdpDescription);
                            PeerConnectionItem item = null;
                            if (mConnections != null) {
                                for (int i = 0; i < mConnections.size(); i++) {
                                    item = mConnections.get(i);
                                    if (socketId != null && socketId.equals(item.socketId)) {
                                        item.remoteSdp = offerSdp;
                                        createAnswer(item);
                                        break;
                                    }
                                }
                            }
                        } else if (type.equals("_answer")) {
                            JSONObject data = json.getJSONObject("data");
                            String socketId = data.getString("socketId");
                            JSONObject sdp = data.getJSONObject("sdp");
                            String sdpDescription = sdp.getString("sdp");
                            String sdpType = sdp.getString("type");
                            SessionDescription answerSdp = new SessionDescription(
                                    SessionDescription.Type.fromCanonicalForm(sdpType.toUpperCase()), sdpDescription);
                            PeerConnectionItem item = null;
                            if (mConnections != null) {
                                for (int i = 0; i < mConnections.size(); i++) {
                                    item = mConnections.get(i);
                                    if (socketId != null && socketId.equals(item.socketId)) {
                                        item.peerConnectionClient.setRemoteDescription(answerSdp);
                                        item.remoteSdp = answerSdp;
                                        break;
                                    }
                                }
                            }
                        } else if (type.equals("_ice_candidate")) {
                            JSONObject data = json.getJSONObject("data");
                            String socketId = data.getString("socketId");
                            JSONObject candidate = data.getJSONObject("candidate");
                            int sdpMLineIndex = candidate.getInt("sdpMLineIndex");
                            String sdpMid = candidate.getString("sdpMid");
                            String sdp = candidate.getString("candidate");
                            IceCandidate iceCandidate = new IceCandidate(sdpMid, sdpMLineIndex, sdp);
                            PeerConnectionItem item = null;
                            if (mConnections != null) {
                                for (int i = 0; i < mConnections.size(); i++) {
                                    item = mConnections.get(i);
                                    if (socketId != null && socketId.equals(item.socketId)) {
                                        item.peerConnectionClient.addRemoteIceCandidate(iceCandidate);
                                        break;
                                    }
                                }
                            }
                        } else if (type.equals("_remove_peer")) {
                            JSONObject data = json.getJSONObject("data");
                            String socketId = data.getString("socketId");
                            PeerConnectionItem item = null;
                            if (mConnections != null) {
                                for (int i = 0; i < mConnections.size(); i++) {
                                    item = mConnections.get(i);
                                    if (socketId != null && socketId.equals(item.socketId)) {
                                        if (item != null && item.peerConnectionClient != null) {
                                            item.peerConnectionClient.close();
                                            item.peerConnectionClient = null;
                                        }
                                        mConnections.remove(i);
                                        break;
                                    }
                                }
                            }
                        } else if (type.equals("_pong")) {
                            JSONObject data = json.getJSONObject("data");
                            String socketId = data.getString("socketId");
                            if (socketId != null && socketId.equals(mSocketId)) {
                                mHandler.removeMessages(MessageHandler.MSG_RECALL);
                                mHandler.sendEmptyMessageDelayed(MessageHandler.MSG_SEND_STATE, MessageHandler.SEND_STATE_DELAYED);
                                mHandler.sendEmptyMessageDelayed(MessageHandler.MSG_RECALL, MessageHandler.RESTART_DELAYED);
                            }
                        } else if (type.equals("_set_wifi")) {/*设置wifi自动连接*/
                            JSONObject data = json.getJSONObject("data");
                            JSONObject wifiInfo = data.getJSONObject("wifiInfo");
                            String wifi_name = wifiInfo.getString("wifi_name");
                            String wifi_mima = wifiInfo.getString("wifi_mima");
                            Log.d("chunlei", "wifi_name: " + wifi_name);
                            //   AddWifiConfig(wifi_name, wifi_mima);
                            setDefaultWifi(wifi_name, wifi_mima);
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "WebSocket message JSON parsing error: ", e);
                    }
                }
            }
        });
    }

    @Override
    public void onWebSocketClose() {
        Log.e(TAG, "onWebSocketClose()...");
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                isWebSocketConnected = false;
                disconnect("WebSocket close.");
                mHandler.sendEmptyMessage(MessageHandler.MSG_RECALL);
            }
        });
    }

    @Override
    public void onWebSocketError(String description) {
        reportError("onWebSocketError=>" + description);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
//                isWebSocketConnected = false;
                disconnect("WebSocket error.");
                mHandler.sendEmptyMessage(MessageHandler.MSG_RECALL);
            }
        });
    }

    /*----------------PeerConnectionClient.PeerConnectionEvents---------------*/
    @Override
    public void onLocalDescription(SessionDescription sdp, String socketId) {
        Log.d(TAG, "onLocalDescription=>sockedId: " + socketId);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (mLock) {
                    PeerConnectionItem item = null;
                    if (mConnections != null) {
                        for (int i = 0; i < mConnections.size(); i++) {
                            item = mConnections.get(i);
                            if (socketId != null && socketId.equals(item.socketId) && item.localSdp == null) {
                                Log.d(TAG, "send offer sdp. socketId: " + socketId);
                                item.localSdp = sdp;
                                if (mVideoStartBitrate > 0) {
                                    Log.d(TAG, "Set video maximum bitrate: " + mVideoStartBitrate);
                                    item.peerConnectionClient.setVideoMaxBitrate(mVideoStartBitrate);
                                }
                                sendOfferSdp(item);
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onIceCandidate(IceCandidate candidate, String socketId) {
        Log.d(TAG, "onIceCandidate=>sockedId: " + socketId);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (mLock) {
                    PeerConnectionItem item = null;
                    if (mConnections != null) {
                        for (int i = 0; i < mConnections.size(); i++) {
                            item = mConnections.get(i);
                            if (socketId != null && socketId.equals(item.socketId)) {
                                sendIceCandidate(candidate, socketId);
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onIceCandidatesRemoved(IceCandidate[] candidates, String socketId) {
        Log.d(TAG, "onIceCandidatesRemoved=>sockedId: " + socketId);
    }

    @Override
    public void onIceConnected(String socketId) {
        Log.d(TAG, "onIceConnected=>sockedId: " + socketId);
        isPeerConnected = true;
        mHandler.removeMessages(MessageHandler.MSG_CONNECTED_TIMEOUT);
    }

    @Override
    public void onIceDisconnected(String socketId) {
        Log.d(TAG, "onIceDisconnected=>sockedId: " + socketId);
    }

    @Override
    public void onPeerConnectionClosed(String socketId) {
        Log.d(TAG, "onPeerConnectionClosed=>sockedId: " + socketId);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (mLock) {
                    PeerConnectionItem item = null;
                    if (mConnections != null) {
                        for (int i = 0; i < mConnections.size(); i++) {
                            item = mConnections.get(i);
                            if (socketId != null && socketId.equals(item.socketId)) {
                                mConnections.remove(i);
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onPeerConnectionStatsReady(StatsReport[] reports, String socketId) {
        Log.d(TAG, "onPeerConnectionStatsReady=>sockedId: " + socketId);
    }

    @Override
    public void onPeerConnectionError(String description, String socketId) {
        reportError("onPeerConnectionError=>description: " + description + " sockedId: " + socketId);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                disconnect("Peer connection error.");
                mHandler.sendEmptyMessage(MessageHandler.MSG_RECALL);
            }
        });
    }
    /*-------------WebSocketClient.WebSocketChannelEvents-------------*/

    private static class MessageHandler extends Handler {

        private static final int MSG_RECALL = 0;
        private static final int MSG_SEND_STATE = 1;
        private static final int MSG_CONNECTED_TIMEOUT = 2;

        private static final int RECALL_DELAYED = 1000;
        private static final int SEND_STATE_DELAYED = 8000;
        private static final int RESTART_DELAYED = 15000;
        private static final int CONNECTED_TIMEOUT = 30000;

        private AppRTCService mService;

        public MessageHandler(AppRTCService service, Looper looper) {
            super(looper);
            mService = service;
        }

        @Override
        public void handleMessage(Message msg) {
            Log.d(AppRTCService.TAG, "handleMessage=>what: " + msg.what);
            removeMessages(msg.what);
            switch (msg.what) {
                case MSG_CONNECTED_TIMEOUT:
                    mService.disconnect("connected timeout");
                case MSG_RECALL:
                    if (mService.isNetworkActive()) {
                        if (!mService.isWebSocketConnected) {
                            postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mService.startCall();
                                }
                            }, 3000);
                        } else {
                            Log.d(TAG, "wait disconnected finish.");
                        }
                    } else {
                        Log.d(TAG, "handleMessage=>network not active.");
                        sendEmptyMessageDelayed(MSG_RECALL, RECALL_DELAYED);
                    }
                    break;

                case MSG_SEND_STATE:
                    mService.sendHeartbeat();
                    break;

            }
        }
    }

    private static class ProxyVideoSink implements VideoSink {
        private VideoSink target;

        @Override
        synchronized public void onFrame(VideoFrame frame) {
            if (target == null) {
                //Log.d(TAG, "Dropping frame in proxy because target is null.");
                return;
            }

            target.onFrame(frame);
        }

        synchronized public void setTarget(VideoSink target) {
            this.target = target;
        }
    }

    public class PeerConnectionItem {
        public boolean isOffer;
        public String socketId;
        public PeerConnectionClient peerConnectionClient;
        public SessionDescription localSdp;
        public SessionDescription remoteSdp;
    }
}