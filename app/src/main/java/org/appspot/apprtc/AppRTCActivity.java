package org.appspot.apprtc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

public class AppRTCActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "AppRTCActivity";

    private Button mStartServiceBtn;
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_rtc);

        mStartServiceBtn = (Button)findViewById(R.id.start_server);

        mStartServiceBtn.setOnClickListener(this);

        mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "wakeup");
        mWakeLock.acquire();
        mWakeLock.release();
    }

    @Override
    public void onClick(View v) {
        Intent service = new Intent(this, AppRTCService.class);
        startService(service);
    }
}
