package org.appspot.apprtc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkStatuReceiver extends BroadcastReceiver {

    private static final String TAG = "NetworkStatuReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive=>action: " + intent.getAction());
        Intent service = new Intent(context, AppRTCService.class);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null) {
            if (info.isAvailable() && info.isConnected()) {
                Log.d(TAG, "onReceive=>network active.");
//                context.startService(service);
            } else {
                Log.d(TAG, "onReceive=>network not active.");
//                context.stopService(service);
            }
        } else {
            Log.d(TAG, "onReceive=>network not active.");
//            context.stopService(service);
        }
    }
}
